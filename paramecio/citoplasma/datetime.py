import time
from datetime import date, datetime, tzinfo
import arrow
# from babel.dates import format_date, format_datetime, format_time, get_timezone, UTC
try:
    from settings import config
except:
    config={}
#from paramecio.citoplasma.sessions import get_session
from os import environ

"""Simple hook for timedate functions from Arrow datetime module. Maybe in the future use native python datetime functions or other libraries. Is simply an abstraction for not depend of particular library.
"""

sql_format_time='YYYYMMDDHHmmss'
"""str: variable for define basic string for format dates


By default, datetime module use YYYYMMDDHHmmss string for define dates and time. Tipically is used for sql operations in paramecio2 framework.
"""

format_date_txt="YYYY/MM/DD"
"""str: variable for define basic formatted date string
"""

format_time_txt="HH:mm:ss"
"""str: variable for define basic formatted time string
"""

timezone='Europe/Madrid'
"""str: basic timezone for dates, by default, Europe/Madrid
"""

"""If default are changed in settings/config, change variables
"""

if hasattr(config, 'format_date'):
    format_date_txt=config.format_date

if hasattr(config, 'format_time'):
    format_time_txt=config.format_time

if hasattr(config, 'timezone'):
    timezone=config.timezone    

def set_timezone():
    """Simple function for change the timezone in general environment of python
    """
    
    environ['TZ']=environ.get('TZ', timezone)
    
    if environ['TZ']!=timezone:
        environ['TZ']=timezone
        time.tzset()

"""
def set_timezone_session():
    
    s=get_session()
    
    timezone_local=timezone
    
    if s!=None:
        if 'timezone' in s:
            timezone_local=s['timezone']
        #timezone_local=s.get('timezone', timezone)
    
    environ['TZ']=environ.get('TZ', timezone_local)
    
    if environ['TZ']!=timezone_local:
        environ['TZ']=timezone_local
        time.tzset()
    
    #request.environ['TIMEZONE'] = request.environ['PATH_INFO'].rstrip('/')
"""

def format_timedata(time):
    """Function for get separated year, month, day, hour, minute and second from sql_format_time string
    
    Args:
        time (str): A YYYYMMDDHHmmss string for get datetime components from there.
     
    Returns:
        list: A dict with datetime components (year, month, day, hour, minute, second).
    
    """
    
    year=0
    month=0
    day=0
    hour=0
    minute=0
    second=0
    ampm=''
    
    try:
    
        year=int(time[:4])
        
        month=int(time[4:6])
        
        day=int(time[6:8])
        
        hour=int(time[8:10])
        
        minute=int(time[10:12])
        
        second=int(time[12:14])
    
        ampm=int(time[14:16])

    except:
        pass
    
    if ampm=='PM' or ampm=='pm':
        
        if hour>0:
            hour+=12

    return (year, month, day, hour, minute, second)

def checkdatetime(y, m, d, h, mi, s):
    """Check if a series of datetime separated elements are correct, the datetime values are type int
    
    Args:
        y (int): Year of datetime
        m (int): month 
        d (int): day
        h (int): hour
        mi (int): minute
        s (int): seconds
        
    Returns:
        bool: If values are correct, return True, otherwise return False
    
    """
    
    try:
        #test=datetime.strptime(str(y)+'-'+str(m)+'-'+str(d)+' '+str(h)+'-'+str(mi)+'-'+str(s), '%Y-%m-%d %H-%M-%S')
        test=arrow.arrow.Arrow(y, m, d, h, mi, s)
        return True
    except:
        return False
    
# Get the localtime
    
def now(utc=False, tz=''):
    
    """Returns the actual datetime in YYYYMMDDHHmmss format. 
    
    Args:
        utc (bool): If True, the datetime is returned in UTC timezone
        tz (str, optional): Timezone name, example: Europe/Madrid. If set the datetime is returned in the timezone selected
    
    Returns:
        str: Return actual datetime
    
    """
        
    if not utc:
        if tz=='':
            actual=arrow.now().format(sql_format_time)
        else:
            #actual=arrow.to(tz).now().format(sql_format_time)
            utc=arrow.utcnow()
            actual=utc.to(tz).format(sql_format_time)
    else:
        actual=arrow.utcnow().format(sql_format_time)
    
    return actual
    
def today(utc=False,tz=''):
    
    """Returns the actual date in YYYYMMDDHHmmss format. 
    
    Is different from (now) function because return the date to 00:00:00 time
    
    Args:
        utc (bool): If True, the date is returned in UTC timezone
        tz (str, optional): Timezone name, example: Europe/Madrid. If set the date is returned in the timezone selected
    
    Returns:
        str: Return actual date with 00:00:00 how time
    
    """
    
    return now(utc, tz)[:8]+'000000'
    
# Get actual timestamp

def obtain_timestamp(timeform):
    
    """Get the timestamp from datetime in YYYYMMDDHHmmss format. 
    
    Args:
        timeform (str): Datetime in YYYYMMDDHHmmss format. 
    
    Returns:
    
        int: datetime in timestamp format
    
    """
    
    y, m, d, h, mi, s=format_timedata(timeform)
    
    if checkdatetime(y, m, d, h, mi, s):
        
        #timestamp=int(time.mktime((y, m, d, h, mi, s, 0, 0, -1)))       
        
        timestamp=arrow.arrow.Arrow(y, m, d, h, mi, s).timestamp()
         
        return timestamp
        
        #return mktime($h, $mi, $s, $m, $d, $y);
    else:
        return False

# timestamp is gmt time, convert in normal time

def timestamp_to_datetime(timestamp, sql_format_time=sql_format_time):
    
    """Turn datetime in YYYYMMDDHHmmss format. 
    
    Args:
        timestamp (int): The timestamp for convert
        
    Returns:
        str: Datetime in YYYYMMDDHHmmss format
    
    """
    
    return arrow.get(timestamp).format(sql_format_time)

# Get a utc timestamp and convert to local

def timestamp_to_datetime_local(timestamp, tz='', sql_format_time=sql_format_time):
    """Get a utc timestamp and convert to timezone datetime in YYYYMMDDHHmmss format. 
    
    Args:
        timestamp (int): The timestamp for convert in datetime
        tz (str, optional): If you want convert to other timezone, set it.
        
    Returns:
    
        str: Datetime in YYYYMMDDHHmmss format in selected timezone datetime
    
    """
    
    t=arrow.get(timestamp)

    if tz=='':
        tz=environ['TZ']    
    
    return t.to(tz).format(sql_format_time)


def format_datetime(format_time, timeform, func_utc_return):
    
    """Get a datetime in YYYYMMDDHHmmss format and convert in other str datetime (normally, same YYYYMMDDHHmmss format). Is a primitive function for other high level datetime functions. 
    
    Args:
        format_time (str): The strtime string used for format the datetime
        timeform (str): datetime in YYYYMMDDHHmmss format to convert to new format
        func_utc_return (function): A function used for get the datetime.
        
    Returns:
        If timestamp is False, return False, if timestamp is valid, return the datetime formatted 
        
    """
    
    timestamp=obtain_timestamp(timeform)
    
    if timestamp:
    
        t=func_utc_return(timestamp)
        
        return t.format(format_time)
        
    else:
    
        return False
        
# This method parse local time to gmt

def local_to_gmt(timeform, sql_format_time=sql_format_time):
    
    """Get a datetime in YYYYMMDDHHmmss format and convert in other str datetime. Is a primitive function for other high level datetime functions. 
    
    Expects that timeform was in time not gmt and convert to gmt
    
    Args:
        timeform (str): datetime in YYYYMMDDHHmmss format to convert to new format
        sql_format_time (str, optional): by default, the format is YYYYMMDDHHmmss, you can put other formatted str formats for date, here. 
        
    Returns:
        If timeform is False, return False, if timeform is valid, return the datetime formatted 
        
    """
    
    return format_datetime(sql_format_time, timeform, substract_utc)

# time.localtime is useless, you need sum the time offset to the date

def gmt_to_local(timeform, sql_format_time=sql_format_time):
    
    """Get a datetime in YYYYMMDDHHmmss format in UTC and convert in other str datetime. Is a primitive function for other high level datetime functions. 
    
    Expects that timeform was in time gmt and convert to localtime
    
    Args:
        timeform (str): datetime in YYYYMMDDHHmmss format to convert to new format
        sql_format_time (str, optional): by default, the format is YYYYMMDDHHmmss, you can put other formatted str formats for date, here. 
        
    Returns:
        If timeform is False, return False, if timeform is valid, return the datetime formatted 
    
    """
    
    return format_datetime(sql_format_time, timeform, sum_utc)

def format_time(timeform):
    
    """Get a datetime in YYYYMMDDHHmmss format and convert in HH:mm:ss UTC format. Is a primitive function for other high level datetime functions. 
    
    Args:
        timeform (str): datetime in YYYYMMDDHHmmss format to convert to new format
        
    Returns:
        If timeform is False, return False, if timeform is valid, return the datetime formatted in UTC
        
    """
    
    return format_datetime(format_time_txt, timeform, sum_utc)

def format_date(timeform):
    
    """Get a datetime in YYYYMMDDHHmmss format and convert in YYYY/MM/DD UTC format. Is a primitive function for other high level datetime functions. 
    
    Args:
        timeform (str): datetime in YYYYMMDDHHmmss format to convert to new format
        
    Returns:
        If timeform is False, return False, if timeform is valid, return the datetime formatted in UTC
    """
    
    return format_datetime(format_date_txt, timeform, sum_utc)

def format_fulldate(timeform):
    
    """Get a datetime in YYYYMMDDHHmmss format and convert in YYYY/MM/DD HH:mm:ss UTC format. Is a primitive function for other high level datetime functions. 
    
    Args:
        timeform (str): datetime in YYYYMMDDHHmmss format to convert to new format
        
    Returns:
        If timeform is False, return False, if timeform is valid, return the datetime formatted in UTC
    """
    
    return format_datetime(format_date_txt+' '+format_time_txt, timeform, sum_utc)

def format_local_time(timeform):
    
    """Get a datetime in YYYYMMDDHHmmss format and convert in HH:mm:ss format. Is a primitive function for other high level datetime functions. 
    
    Args:
        timeform (str): datetime in YYYYMMDDHHmmss format to convert to new format
        
    Returns:
        If timeform is False, return False, if timeform is valid, return the datetime formatted 
    """
    
    return format_datetime(format_time_txt, timeform, no_utc)

def format_local_date(timeform):
    
    """Get a datetime in YYYYMMDDHHmmss format and convert in YYYY/MM/DD format. Is a primitive function for other high level datetime functions. 
    
    Args:
        timeform (str): datetime in YYYYMMDDHHmmss format to convert to new format
        
    Returns:
        If timeform is False, return False, if timeform is valid, return the datetime formatted 
    """
    
    return format_datetime(format_date_txt, timeform, no_utc)

def format_local_fulldate(timeform):
    
    """Get a datetime in YYYYMMDDHHmmss format and convert in YYYY/MM/DD HH:mm:ss format. Is a primitive function for other high level datetime functions. 
    
    Args:
        timeform (str): datetime in YYYYMMDDHHmmss format to convert to new format
        
    Returns:
        If timeform is False, return False, if timeform is valid, return the datetime formatted 
    """
    
    return format_datetime(format_date_txt+' '+format_time_txt, timeform, no_utc)

def format_strtime(strtime, timeform):
    
    """Get a datetime in YYYYMMDDHHmmss format and convert in strtime string UTC format. Is a primitive function for other high level datetime functions. 
    
    Args:
        timeform (str): datetime in YYYYMMDDHHmmss format to convert to new format
        
    Returns:
        If timeform is False, return False, if timeform is valid, return the datetime formatted in UTC
    """
    
    return format_datetime(strtime, timeform, sum_utc)    

def format_local_strtime(strtime, timeform):
    
    """Get a datetime in YYYYMMDDHHmmss format and convert in strtime string format. Is a primitive function for other high level datetime functions. 
    
    Args:
        timeform (str): datetime in YYYYMMDDHHmmss format to convert to new format
        
    Returns:
        If timeform is False, return False, if timeform is valid, return the datetime formatted
    """
    
    return format_datetime(strtime, timeform, no_utc)    

#Input is utc timestamp, return local arrow object

def sum_utc(timestamp, tz=''):
    
    """Get timestamp in UTC and convert in arrow date object with timezone datetime
    
    Args:
        timestamp (int): The timestamp for convert in other timezone
        tz (str): Timezone of timestamp
        
    Returns:
        Return arrow object with new timezone selected
    """
    
    #offset=time.altzone

    #return time.localtime(timestamp-offset)
    
    t=arrow.get(timestamp)
    
    if tz=='':
        tz=environ['TZ']    
    
    return t.to(tz)

#Input is local timestamp, return utc arrow object

def substract_utc(timestamp, tz=''):
    
    """Get local timestamp and convert in arrow date object with UTC datetime
    
    Args:
        timestamp (int): The timestamp for convert in UTC timezone
        tz (str): Timezone of timestamp
        
    Returns:
        Return arrow object with UTC timezone selected
    """
    
    #offset=time.altzone

    #return time.localtime(timestamp+offset)
    
    #t=arrow.get(timestamp).to('UTC')
    
    timeform=timestamp_to_datetime(timestamp)
    
    y, m, d, h, mi, s=format_timedata(timeform)

    if tz=='':
        tz=environ['TZ']    
    
    t=arrow.get(datetime(y, m, d, h, mi, s), tz).to('UTC')
    
    return t

def no_utc(timestamp):
    
    """Return an arrow object based in timestamp value
    
    Args:
        timestamp (int): The timestamp for convert in UTC timezone
        
    Returns:
        Return arrow object based in timestamp value
    
    """
    
    return arrow.get(timestamp)

class TimeClass:
    """Simple abstraction of arrow class, in future i can change arrow class by others
    
    Args:
        timestamp (int, str, optional): You can set the initial arrow object with timestamp date or YYYYMMDDHHmmss date format 
        tz (str): Timezone 
    
    Attributes:
        utc (bool): If True, the default timezone is UTC, if False, timezone is system default
        format_time (str): The default datetime format, YYYYMMDDHHmmss
        format_time_txt (str): Time text format, usually HH:mm:ss
        format_date_txt (str): Date text format, usually YYYY/MM/DD
        format_date_full (str): Full DateTime text format, usually YYYY/MM/DD HH:mm:ss
        tz (str): Default timezone for arrow object
    
    """
    
    def __init__(self, timestamp=None, tz=''):
        
        self.utc=False
        
        self.format_time=sql_format_time
        self.format_time_txt=format_time_txt

        self.format_date_txt=format_date_txt

        self.format_date_full=format_date_txt+' '+format_time_txt
        
        self.tz=environ.get('TZ', 'utc')
        
        if tz:
            self.tz=tz
        
        if type(timestamp).__name__=='int':

            self.datetime=timestamp_to_datetime(timestamp)        

        else:
            
            if not timestamp:
            
                self.datetime=now(self.utc, tz)
            
            else:
                self.datetime=timestamp
        
        y, m, d, h, mi, s=format_timedata(self.datetime)
        
        self.t=arrow.get(datetime(y, m, d, h, mi, s), self.tz)
    
    def add_month(self, num_months):
        """Method for add months to datetime
        
        Args:
            num_months (int): Number of months to add
            
        Returns:
            New added datetime
        """
        
        m=self.t.shift(months=+num_months)
        
        return m.format(self.format_time)

    def substract_month(self, num_months):
        """Method for substract months to datetime
        
        Args:
            num_months (int): Number of months to substract
        
        Returns:
            New substracted datetime
        """
        
        m=self.t.shift(months=-num_months)
        
        return m.format(self.format_time)

    def add_day(self, num_days):
        """Method for add days to datetime
        
        Args:
            num_days (int): Number of days to add
        
        Returns:
            New added datetime
        """
        
        m=self.t.shift(days=+num_days)
        
        return m.format(self.format_time)

    def substract_day(self, num_days):
        """Method for substract days to datetime
        
        Args:
            num_days (int): Number of days to substract
        
        Returns:
            New substracted datetime
        
        """
        
        m=self.t.shift(days=-num_days)
        
        return m.format(self.format_time)

    def add_year(self, num_years):
        """Method for add years to datetime
        
        Args:
            num_years (int): Number of years to add
        
        Returns:
            New added datetime
        """
        
        m=self.t.shift(years=+num_years)
        
        return m.format(self.format_time)

    def substract_year(self, num_years):
        """Method for substract years to datetime
        
        Args:
            num_years (int): Number of years to substract
        
        Returns:
            New substracted datetime
        """
        
        m=self.t.shift(years=-num_years)
        
        return m.format(self.format_time)

    def add_hour(self, num_hours):
        """Method for add hours to datetime
        
        Args:
            num_hours (int): Number of hours to add
            
        Returns:
            New added datetime
        """
        
        m=self.t.shift(hours=+num_hours)
        
        return m.format(self.format_time)

    def substract_hour(self, num_hours):
        """Method for substract hours to datetime
        
        Args:
            num_hours (int): Number of hours to substract
            
        Returns:
            New substracted datetime
        """
        
        m=self.t.shift(hours=-num_hours)
        
        return m.format(self.format_time)
    
    def format(self):
        """Method for get datetime formatted using format_date_full attribute
        
        Returns:
            Datetime formatted with format_date_full attribute
        """
        
        return self.t.format(self.format_date_full)
    
    def local_to_utc(self):
        """Method for convert datetime from actual timezone to UTC"""
        
        self.t=self.t.to('utc')

    # Only use 

    def utc_to_local(self):
        """Method for convert datetime from actual timezone from UTC to actual timezone"""
        
        self.t=self.t.to(self.tz)

    def local_to_tz(self, tz):
        """Method for convert actual timezone to other timezone"""
        
        self.t=self.t.to(tz)

    def now(self, utc=False):        
        """Method for get actual datetime.
        
        Args:
            utc (bool): If True, then get actual datetime in UTC datetime, if False, get actual datetime in selected timezone in tz attribute
        
        Returns:
        
            Actual datetime formatted in YYYYMMDDHHmmss format. 
        """
        
        if not utc:

            actual=arrow.now(self.tz).format(sql_format_time)
        else:
            actual=arrow.utcnow().format(sql_format_time)
        
        return actual
        
    def today(self, utc=False):
        
        """Method for get today datetime. Get now datetime with 00:00:00 time. 
        
        Args:
            utc (bool): If True, then get actual datetime in UTC datetime, if False, get actual datetime in selected timezone in tz attribute
        
        Returns:
        
            Actual datetime formatted in YYYYMMDD000000 format. 
        """
        
        if utc:
        
            return arrow.utcnow()[:8]+'000000'
        else:
            return arrow.now(self.tz)[:8]+'000000'            

    def timestamp_to_datetime(self, timestamp):
        
        """Method for convert a timestamp in YYYYMMDDHHmmss format. 
        
        Args:
        
            timestamp (int): datetime in timestamp format.
            
        Returns:
        
            Datetime in YYYYMMDDHHmmss format. 
        """
        
        return arrow.get(timestamp).format(sql_format_time)

    def obtain_timestamp(self, timeform):
        
        """Method for get timestamp from a datetime in YYYYMMDDHHmmss format. 
        
        Args:
            timeform (str): Datetime in YYYYMMDDHHmmss format. 
        
        Returns:
            
            Datetime in YYYYMMDDHHmmss format.If timeform is incorrect, return False
        """
        
        y, m, d, h, mi, s=format_timedata(timeform)
        
        if checkdatetime(y, m, d, h, mi, s):
            
            timestamp=arrow.arrow.Arrow(y, m, d, h, mi, s).timestamp                
            
            return timestamp
            
        else:
            return False
            
    def format_strtime(self, strtime, timeform):
    
        """Method for get datetime formatted in strtime format
        
        Args:
            strtime (str): The string used for format the datetime
            timeform (str): Datetime in YYYYMMDDHHmmss format. 
            
        Returns:
            
            Datetime in strtime format.If timeform is incorrect, return False
        
        """
        
        
        try:
            y, m, d, h, mi, s=format_timedata(timeform)

            return arrow.get(datetime(y, m, d, h, mi, s), self.tz).format(strtime)
            
        except:
        
            return False
